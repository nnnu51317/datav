"""
线图 + zoom滑块在左侧, logY效果
GDP数据取 北京、山东、广东、河南、广西、甘肃 六个省，所有年份
线图 Y轴log效果，设置每个省使用的点用不同形状，数据紧贴Y轴。横向zoom缩放条在下方，滑块靠左。
"""
import pandas as pd
import numpy as np
from pyecharts.charts import Line
from pyecharts import options as opts

df = pd.read_csv('gdp.csv')
d = df.iloc[[0,14,15,18,19,27],0:].set_index("province")
d = d.T.sort_index()

x =  d.index.tolist() #X轴
y = np.array(d.T) #Y轴
City = d.columns.values.tolist() #城市
#'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
symbol=['circle','rect','roundRect','triangle','diamond','pin']

#线图
def show_line():
    line = Line().add_xaxis(x)

    for i in range(len(City)):
        if i == 3 or i ==5:
            line.add_yaxis(City[i], list(y[i]),linestyle_opts=opts.LineStyleOpts( width=4,type_='dashed'))
        line.add_yaxis(City[i],list(y[i]),is_smooth=True,symbol=symbol[i],symbol_size=10)
    line.set_global_opts(title_opts=opts.TitleOpts(title="线性滑块"),
                         xaxis_opts=opts.AxisOpts(
                             axistick_opts=opts.AxisTickOpts(is_align_with_label=True),
                             is_scale=False,
                             boundary_gap=False,
                         ),
                         datazoom_opts=opts.DataZoomOpts(pos_left = True,range_start=100),
                         yaxis_opts=opts.AxisOpts(type_="log",is_scale=True))
    return line

show_line().render("线图zoom滑块.html")
